package fr.laerce.formations.java.exercice1;

/**
 * Created by hippias on 03/01/2017.
 */
public class Voiture {
    private String marque;
    private int nombreCheveaux;

    public Voiture(String marque, int nombreCheveaux) {
        this.marque = marque;
        this.nombreCheveaux = nombreCheveaux;
    }

    public String getMarque() {
        return marque;
    }

    public int getNombreCheveaux() {
        return nombreCheveaux;
    }

    public void setMarque(String marque) {
        this.marque = marque;
    }

    public void setNombreCheveaux(int nombreCheveaux) {
        this.nombreCheveaux = nombreCheveaux;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Voiture voiture = (Voiture) o;

        if (nombreCheveaux != voiture.nombreCheveaux) return false;
        return marque.equals(voiture.marque);

    }

    @Override
    public int hashCode() {
        int result = marque.hashCode();
        result = 31 * result + nombreCheveaux;
        return result;
    }
}
