package fr.laerce.formations.java;

import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Created by hippias on 03/01/2017.
 */
//Ces questions portent sur les expressions régulières
    // Vous trouverez une syntaxe de référence ici: https://msdn.microsoft.com/fr-fr/library/ae5bf541(v=vs.100).aspx
public class Exercice2Test {

    @Test
    public void expression__1 () {
        String regex = "";

        assertThat("hello").matches(regex);

        assertThat("allo").doesNotMatch(regex);

        assertThat(regex.length() < 5).isTrue();
        assertThat(regex).doesNotContain(".");
    }

    @Test
    public void expression__2 () {
        String regex = "";

        assertThat("helllllo").matches(regex);
        assertThat("helo").matches(regex);

        assertThat("heo").doesNotMatch(regex);

        assertThat(regex.length() < 5).isTrue();
        assertThat(regex).doesNotContain(".");
    }

    @Test
    public void expression__3 () {
        String regex = "";

        assertThat("heo").matches(regex);
        assertThat("helo").matches(regex);
        assertThat("hellllllo").matches(regex);
        assertThat("hello").matches(regex);

        assertThat("he").doesNotMatch(regex);
        assertThat("ho").doesNotMatch(regex);

        assertThat(regex.length() < 5).isTrue();
        assertThat(regex).doesNotContain(".");
    }

    @Test
    public void expression__4 () {
        String regex = "";

        assertThat("helo").matches(regex);
        assertThat("hello").matches(regex);
        assertThat("helllo").matches(regex);

        assertThat("heo").doesNotMatch(regex);
        assertThat("hellllo").doesNotMatch(regex);

        assertThat(regex.length() < 8).isTrue();
        assertThat(regex).doesNotContain(".");
    }

    @Test
    public void expression__5 () {
        String regex = "";

        assertThat("he  llo").matches(regex);
        assertThat("he llo").matches(regex);
        assertThat("hello").matches(regex);

        assertThat("he  lo").doesNotMatch(regex);

        assertThat(regex.length() < 8).isTrue();
        assertThat(regex).doesNotContain(".");
    }

    @Test
    public void expression__6 () {
        String regex = "";

        assertThat("12abc45cd").matches(regex);
        assertThat("64a23hufehfe").matches(regex);
        assertThat("1a23hufehfe").matches(regex);
        assertThat("3a2h").matches(regex);

        assertThat("a34fe").doesNotMatch(regex);
        assertThat("afe").doesNotMatch(regex);
        assertThat("1fea4").doesNotMatch(regex);

        assertThat(regex.length() < 12).isTrue();
        assertThat(regex).doesNotContain(".");
    }

    @Test
    public void expression__7 () {
        String regex = "";

        assertThat("   ioufouhebqu fheqhoufeq  huo").matches(regex);
        assertThat("  huo").matches(regex);
        assertThat(" 7979t431 9yr7 931g   huo").matches(regex);
        assertThat("huo").matches(regex);

        assertThat("  uo").doesNotMatch(regex);
        assertThat("uo").doesNotMatch(regex);
        assertThat("huo   ").doesNotMatch(regex);

        assertThat(regex.length() < 6).isTrue();
    }

    @Test
    public void expression__8 () {
        String regex = "";

        assertThat("10.2.3").matches(regex);
        assertThat("1.20.3").matches(regex);
        assertThat("10.255.3").matches(regex);
        assertThat("10.155.3").matches(regex);
        assertThat("255.255.3").matches(regex);
        assertThat("155.255.3").matches(regex);
        assertThat("10.2.35").matches(regex);
        assertThat("10.2.0").matches(regex);
        assertThat("0.0.0").matches(regex);

        assertThat("300.34.54").doesNotMatch(regex);
        assertThat("300.34.").doesNotMatch(regex);
        assertThat("300.34.ad").doesNotMatch(regex);
        assertThat("3.34.200").doesNotMatch(regex);
        assertThat("3..200").doesNotMatch(regex);
        assertThat("3.a.200").doesNotMatch(regex);
        assertThat("..200").doesNotMatch(regex);
        assertThat(".25.58").doesNotMatch(regex);
    }

}
