/**
 * Created by hippias on 03/01/2017.
 */
package fr.laerce.formations.java;


import fr.laerce.formations.java.exercice1.Voiture;
import fr.laerce.formations.java.exercice1.VoitureNbChevauxCroissantComparator;
import org.junit.Test;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import static org.assertj.core.api.Assertions.assertThat;

public class Exercice1Test {

    @Test
    public void cestVraimentNul() {
        //Ce test ne passera pas, vous devrez utiliser un bloc try catch pour le faire passer
        //https://www.jmdoudoux.fr/java/dej/chap-exceptions.htm (pour la référence)
        List objets = null;
        objets.add("hello");
    }

    @Test
    public void doitContenirLesElements() {
        List objets = null; //Utiliser une ArrayList

        assertThat(objets).containsExactly("hello", "world");
    }

    @Test
    public void neDoitContenirQueDesString() {
        List objets = null; // reprenez ici le code du test précédent


        objets.add(23);

        assertThat(objets).hasOnlyElementsOfType(String.class); //Comment cela se fait-il que ce test échoue?
    }

    @Test
    public void crerUneListeDeVoiture() {
        List voitures = null;

        //utiliser la syntaxe <Class> pour faire passer le test
        // il faut ajouter trois voitures à cette collection ensuite

        assertThat(voitures.getClass().isAssignableFrom(Voiture.class)).isTrue();
        assertThat(voitures.getClass().isAssignableFrom(String.class)).isFalse();
        assertThat(voitures.size() == 3).isTrue();
    }

    @Test(expected = IllegalArgumentException.class)
    //Utiliser l'instruction throw
    public void empecherLaCreationDuneVoitureAvecUneMarqueNulle () {
        new Voiture(null, 3);
    }

    @Test(expected = IllegalArgumentException.class)
    //Utiliser l'instruction throw
    public void empecherLaCreationDuneVoitureAvecUnNombreDeChevauxA0OuNegatif() {
        new Voiture("Volvo", -1);
        new Voiture("Cadillac", 0);
    }

    @Test
    public void renversonsLEgalite() {
        Voiture v1 = new Voiture("Volvo", 2);
        Voiture v2 = new Voiture("Volvo", 3);
        //Il faut modifier la méthode equals de Voiture


        assertThat(v1 == v2).isFalse(); // ce test est passant car les références ne sont pas égales
        assertThat(v1.equals(v2)).isTrue();
        assertThat(v1).isSameAs(v2);
    }


    @Test
    public void laCollectionDoitEtreTrieeParOrdreCroissantDeNbDeChevaux() {
        Voiture v1 = new Voiture("Volvo", 3);
        Voiture v2 = new Voiture("Audi", 6);
        Voiture v3 = new Voiture("Peugeot", 0);
        Voiture v4 = new Voiture("Citroen", 4);
        Voiture v5 = new Voiture("Suzuki", 1);
        Voiture v6 = new Voiture("BMW", 2);

        List<Voiture> voitures = Arrays.asList(v1, v2, v3, v4, v5, v6);

        Collections.sort(voitures, new VoitureNbChevauxCroissantComparator()); //changez le code de la classe

        assertThat(voitures).containsExactly(v3, v5, v6, v1, v4, v2);
    }

    @Test
    public void filtrerLaCollectionAvecInstructionFor() {
        Voiture v1 = new Voiture("Volvo", 3);
        Voiture v2 = new Voiture("Audi", 6);
        Voiture v3 = new Voiture("Peugeot", 0);
        Voiture v4 = new Voiture("Citroen", 4);
        Voiture v5 = new Voiture("Suzuki", 1);
        Voiture v6 = new Voiture("BMW", 2);

        List<Voiture> voitures = Arrays.asList(v1, v2, v3, v4, v5, v6);

        //Vous devez filtrer la collection voitures pour créer les collections ci-dessous
        //Utilisez une instruction de parcours for pour constituer ces collections
        List<Voiture> voituresNombreCheveauxPair = null;
        List<Voiture> voitureAyantUnNomDeMarqueAvecAuMoinsDeuxFoisLaMemeLettre = null;

        assertThat(0 % 2 == 0).isTrue();
        assertThat(voituresNombreCheveauxPair).containsExactlyInAnyOrder(v2, v4, v6, v6);
        assertThat(voitureAyantUnNomDeMarqueAvecAuMoinsDeuxFoisLaMemeLettre).containsExactlyInAnyOrder(v1,v3,v5);
    }

    @Test
    public void filtrerLaCollectionAvecLambda() {
        Voiture v1 = new Voiture("Volvo", 3);
        Voiture v2 = new Voiture("Audi", 6);
        Voiture v3 = new Voiture("Peugeot", 0);
        Voiture v4 = new Voiture("Citroen", 4);
        Voiture v5 = new Voiture("Suzuki", 1);
        Voiture v6 = new Voiture("BMW", 2);

        List<Voiture> voitures = Arrays.asList(v1, v2, v3, v4, v5, v6);

        //Vous devez filtrer la collection voitures pour créer les collections ci-dessous
        //Il s'agit ici de remplacer false par un code qui marche
        List<Voiture> voituresNombreCheveauxPair = voitures.stream().filter(v -> false).collect(Collectors.toList());
        List<Voiture> voitureAyantUnNomDeMarqueAvecAuMoinsDeuxFoisLaMemeLettre = voitures.stream().filter(v -> false).collect(Collectors.toList());

        assertThat(voituresNombreCheveauxPair).containsExactlyInAnyOrder(v2, v4, v6, v6);
        assertThat(voitureAyantUnNomDeMarqueAvecAuMoinsDeuxFoisLaMemeLettre).containsExactlyInAnyOrder(v1,v3,v5);
    }

    @Test
    public void laCollectionDoitEtreTrieeParOrdreCroissantDeNbDeChevauxAvecLambda() {
        Voiture v1 = new Voiture("Volvo", 3);
        Voiture v2 = new Voiture("Audi", 6);
        Voiture v3 = new Voiture("Peugeot", 0);
        Voiture v4 = new Voiture("Citroen", 4);
        Voiture v5 = new Voiture("Suzuki", 1);
        Voiture v6 = new Voiture("BMW", 2);

        List<Voiture> voitures = Arrays.asList(v1, v2, v3, v4, v5, v6);
        //Même exercice que précédemment mais vous devez placer votre code à la place du 0
        voitures.sort((o1, o2) -> 0);

        assertThat(voitures).containsExactly(v3, v5, v6, v1, v4, v2);
    }


}
