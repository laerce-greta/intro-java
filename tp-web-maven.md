# TP - web maven

Au cours de ce TP vous allez créer votre premiére application Web grâce à Maven.

## Initialisation du projet:


* `git clone git@gitcdi.laerce.fr:greta-material/mvn-web.git`

* Observez le contenu du fichier pom.xml, remarquez la valeur de la balise `<packaging>` et la présence d'une balise `<plugins>`
* Exécutez la commande suivante à la racine du projet: `mvn tomcat7:run`
* Ouvrez votre navigateur à l'URL suivante: http://localhost:8080/mvn-web une page Hello World devrait s'afficher
* Modifiez le contenu du fichier `index.jsp` et rafraichissez la page du navigateur

Vous pouvez donc modifier à chaud vos fichiers de présentation pour en voir le résultat sur votre page Web locale.

* Observez le contenu du dossier `target/tomcat/` c'est là où tout est placé pour faire tourner le serveur Tomcat en local

Maintenant vous allez installer un serveur Tomcat en local et y déployer le war obtenu via la commande `mvn package`

### Installer Tomcat

* Récupérer l'archive sur la clé USB ou allez sur l'URL suivante: http://apache.websitebeheerjd.nl/tomcat/tomcat-8/v8.0.30/bin/apache-tomcat-8.0.30.zip
* Dézippez l'archive à l'emplacement de votre choix
* Lancez Tomcat en exécutant le script situé dans `_RACINE_TOMCAT_/bin/startup.bat`

### Pour aller plus loin

* Utiliser le goal `deploy` du plugin tomcat pour déployer le jar sur le serveur Tomcat installé.
* Mettez en place un Servlet ajoutant un contenu paramétrable via l'URL